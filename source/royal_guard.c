//compile and run: gcc game.c -o mygame && ./mygame

#include <stdio.h>


/*########################################################################
########################   GLOBAL VARIABLES   ############################
########################################################################*/

	int user_input_char_limit = 20;
	char user_input[20]; 
	int current_scene = 0;
	int quit = 0;


/*########################################################################
###############################   DATA   #################################
########################################################################*/

	const char scenes[][1561] = {  //1561 = width(64) * height(24) + newlines(24) + ending null value(1)  
		{"################################################################\n################################################################\n#####################   ##   ## # ##   ## ######################\n##################### # ## # ## # ## # ## ######################\n############### #####   ## # ### ###   ## ######################\n##############  ## ##  ### # ### ### # ## ######################\n#######  #####  # ### # ##   ### ### # ##   ###   ##############\n######           #################################   ###########\n###### ##########                                        #######\n######           #################################   ###########\n#######  #####  # ###   ## # ##   ##   ##  ####   ##############\n##############  ## ## #### # ## # ## # ## # ####################\n############### ##### # ## # ##   ##   ## # ####################\n##################### # ## # ## # ##  ### # ####################\n#####################   ##   ## # ## # ##  #####################\n################################################################\n################################################################\n###############                              ###################\n################            [start]           ##################\n#################           [continue]         #################\n##################          [help]              ################\n###################         [quit]               ###############\n####################                              ##############\n################################################################\n"},
{"################################################################\n#                                                              #\n#     A beautiful new day rose above the king's forest as      #\n#     sounds of hooves and the rolling of a cart disturbed     #\n#     the stillness that night had inflicted upon it.          #\n#     Coming to a clearing beside a bridge, the captain        #\n#     motioned to the royal guards and the driver to halt      #\n#     as he inspected the surroundings.                        #\n#                                                              #\n#     [observe captain]            [observe cart]              #\n#                                                              #\n#     [observe bridge]             [wait]                      #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Only recently promoted to the captain of the guard,      #\n#     replacing his deceased leader, the man in charge         #\n#     was eager to get the respect of his peers. He            #\n#     enjoyed his newly gained authority, and showed it        #\n#     off in front of royalty.                                 #\n#                                                              #\n#                                                              #\n#     [observe cart]              [observe bridge]             #\n#                                                              #\n#     [wait]                                                   #\n#                                                              #\n#                                                              #\n#                                                #             #\n#                                             ####             #\n#                                          #######             #\n#                                       ##########             #\n#                                          #######             #\n#                                             ####             #\n#                                                #             #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     The princess was being transported to a location         #\n#     unknown to anyone except the captain. He himself         #\n#     suggested the precaution in order to insure her          #\n#     safety by secrecy.                                       #\n#                                                              #\n#                                                              #\n#     [observe captain]            [observe bridge]            #\n#                                                              #\n#     [wait]                                                   #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                 #            #\n#                                                ##            #\n#                                              ####            #\n#                                            ######            #\n#                                          ########            #\n#                                        ##########            #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     The sturdy wooden bridge connected two parts of the      #\n#     forest which were separated by a raging river that       #\n#     had made precipices and chasms through its middle.       #\n#     Only a few such bridges existed and this was the         #\n#     safest one. Nevertheless, one was advised to be wary     #\n#     around such places because bandits were likely to        #\n#     wait hidden and demand fees of passangers.               #\n#                                                              #\n#                                                              #\n#     [observe captain]            [observe cart]              #\n#                                                              #\n#     [wait]                                                   #\n#                                                              #\n#                                                              #\n#                                          #                   #\n#                                         ###                  #\n#                                       #######                #\n#                                      #########               #\n#                                    #############             #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     After a few moments, the captain turned around and       #\n#     ordered the guards to cross the bridge and secure        #\n#     the other side. One of the senior soldiers suggested     #\n#     to leave a few men with the princess' cart, so the       #\n#     commander told him to make his pick and stay himself.    #\n#     When the guards crosed the bridge, they were attacked    #\n#     by mercenaries. But this was only a diversion because    #\n#     immediately after that, mounted riders rushed out of     #\n#     the forest and assailed the men guarding the cart.       #\n#     One was thrown off his frightened horse, while the       #\n#     others fought a bloody battle, being overwhelmed by      #\n#     the enemy's numbers. Suddenly, the control of the        #\n#     cart was taken from the driver, and the mercenaries      #\n#     left in a hurry, chased only by the captain.             #\n#                                                              #\n#     [hide] Staying hidden in a bush his horse had thrown     #\n#     him into, the forgotten soldier looked at the            #\n#     massacre, knowing that he had to survive by all means    #\n#     in order to inform the king.                             #\n#     [fight] Jumping out of a bush, the dismounted            #\n#     soldier joined the fray.                                 #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Remaining hidden throughout the length of the fight,     #\n#     the young soldier watched his peers and friends          #\n#     getting overwhelmed by the mercenary forces. They        #\n#     stood no chance because they were now surrounded from    #\n#     all sides and could not flee. The group on the other     #\n#     side of the bridge was already dealt with and their      #\n#     cries and the clash of arms subsided. Even more          #\n#     mercenaries were coming across now. The man winced       #\n#     as he looked on.                                         #\n#                                                              #\n#                                                              #\n#     [stay hidden] Despite the distress, the young guard      #\n#     remained in his hiding place.                            #\n#                                                              #\n#     [rush out] With boiling blood surging through his        #\n#     veins, the guard jumped out of the bush.                 #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Maiming a mercenary by hitting him from behind, the      #\n#     young guard made his way towards the center. The         #\n#     senior soldier noticed him and commanded that he turn    #\n#     back and use a nearby horse to pursue the cart.          #\n#                                                              #\n#                                                              #\n#     [stay] The cart was probably already far away, and       #\n#     helping his fellows' survival was more important at      #\n#     the moment.                                              #\n#                                                              #\n#     [follow cart] Ensuring the princess' safety was          #\n#     paramount, and the captain would probably need           #\n#     assistance.                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Having killed everyone, the mercenaries looted any       #\n#     valuables off the bodies and made their way towards      #\n#     the direction where the cart was driven to. Noticing     #\n#     a horse grazing in the thicket, one of the men           #\n#     dismounted and went to get it. Pushing through the       #\n#     bushes, he saw the young soldier and quickly slew        #\n#     him. The mercenary took his horse, and went back         #\n#     to his comrades.                                         #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Not being able to withstand the urge to help his         #\n#     fellows, the young guard rushed out of his hiding        #\n#     place and attacked a mercenary from behind. Killing      #\n#     two unsuspecting men, he beckoned to his comrades        #\n#     telling them to try and break out in his direction.      #\n#     They tried, but were easily cut off by the men who       #\n#     had come across the bridge. Making a valiant stand       #\n#     against all odds, they commanded the young guard to      #\n#     run away. And so he did. He ran and ran as fast as       #\n#     he could. And then he fell into the dirst face-down,     #\n#     and with an arrow in his back, with the mercenaries      #\n#     rejoycing and cheering at the archer.                    #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     In spite his orders, the guard made his way through      #\n#     to the center. An epic fight ensued, and many            #\n#     mercenaries were wounded. The men stood their ground     #\n#     for a long time. Having taken care of the guards on      #\n#     the opposite side of the chasm, a large group of men     #\n#     came across the bridge. Getting impatient, their         #\n#     captain ordered the archers to just shoot the            #\n#     desperate guards. The young man was dumbfounded as       #\n#     he lay in a pool of his own blood, looking across at     #\n#     his superior's contorted face, full of                   #\n#     dissappointment and pain.                                #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     With a determined nod, the young guard mounted a         #\n#     horse and rushed after the princess' cart. Luckily,      #\n#     no one followed him. He rode fast down the nearby        #\n#     road because there was no other way a cart could         #\n#     have gone. After some time, the road led out of the      #\n#     overhanging forest, and opened into a great field on     #\n#     its right side. In the distance, there were two          #\n#     horses grazing near a solitary tree.                     #\n#                                                              #\n#                                                              #\n#     [hide and observe] The horses probably belonged to       #\n#     the mercenaries, so it was prudent to first observe      #\n#     what they are doing.                                     #\n#                                                              #\n#     [rush them] There was no time to waste. Any enemy        #\n#     caught off-guard could be killed or interrogated.        #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     The men appeared to be talking, and stayed under the     #\n#     tree for a long time. Are they sentries perhaps?         #\n#     Would they guide the young guard to the princess'        #\n#     cart? Time passed slowly as the thoughts rushed          #\n#     through his head.                                        #\n#                                                              #\n#                                                              #\n#     [keep observing] The men would surely continue their     #\n#     journey soon. It would only be a short moment before     #\n#     they mount their horses. The best thing to do was to     #\n#     keep observing them and then follow behind.              #\n#                                                              #\n#     [move on] Time was fleeting. Every moment he wasted      #\n#     on observing meant that the trail would run ever         #\n#     colder and the chances of catching up would become       #\n#     slimmer. There was the forest continuing on the left     #\n#     side of the field. He could pass the two characters      #\n#     undetected.                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Forcing the horse into a gallop, he rushed towards       #\n#     the tree. When he got near enough, he spotted the        #\n#     captain and a mercenary. It appeared as if the enemy     #\n#     was handing him a rugged bandit outfit and a cape,       #\n#     and the leader of the guard was putting it on. Now       #\n#     near enough to see their faces, a sudden doubt and       #\n#     rage overcame the young man as he recognized the         #\n#     mercenary who killed a friend of his in the initial      #\n#     onslaught.                                               #\n#                                                              #\n#                                                              #\n#     [attack mercenary] Rushing his horse towards the tree,   #\n#     he brandished his sword and jumped on the mercenary.     #\n#                                                              #\n#     [attack captain] There was no doubt that the captain     #\n#     had betrayed them. And for this he would pay.            #\n#                                                              #\n#     [demand answers] Halting the frothing horse into a       #\n#     skid, the young guard demanded answers from the          #\n#     confused men.                                            #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Hardly expecting such a fearsome attack, the             #\n#     mercenary was quickly dealt with, and the captain        #\n#     stood dumbfounded. But he quickly composed himself       #\n#     and put on his usual authoritative bearing, demanding    #\n#     from the young man to explain himself.                   #\n#                                                              #\n#                                                              #\n#     [threaten captain] 'It is evident that you have much     #\n#     more to explain than I! Why have you betrayed us?!',     #\n#     yelled the young man, thinking of his fallen comrades.   #\n#                                                              #\n#     [aid captain] Cutting him off mid-sentence, the young    #\n#     guard asked frantically: 'Captain, are you unhurt? Has   #\n#     this ruffian forced you to take off your armor? Put it   #\n#     on and let us not tally! We can still catch the cart!'   #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     The young guard rushed him, and managed to wound his     #\n#     superior. Had the mercenary not been there to save       #\n#     him, the story might have turned out differently. But    #\n#     two against one was too much, and the young man soon     #\n#     found himself lying on the floor in a pool of his own    #\n#     blood. Before he was allowed to die, the captain         #\n#     kicked and hacked at his body until he was satisfied     #\n#     with the punishment for being attacked.                  #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     There was silence as the two men looked at each other.   #\n#     Finally, the captain said that he managed to bribe       #\n#     the mercenary into helping him infiltrate their camp.    #\n#     He then ordered the perplexed guard to take off any      #\n#     engraved armor and put on a layer of mercenary rags.     #\n#     As the young man complied, and bent down to pick a       #\n#     worn leather vest from a pile of garments, he suddenly   #\n#     found a knife stuck in his throat. As he fell to the     #\n#     ground, he heard the mercenary congratulating the        #\n#     captain on his ruthlessness. 'He was trash, just like    #\n#     the rest of them. I hope they all died.', the man        #\n#     replied with a smirk as he looked down upon the          #\n#     dying lad, choking on his own blood.                     #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     'I need not explain myself to you, boy!', growled the    #\n#     captain as he quickly put on some armor and grabbed      #\n#     his sword and knife. Though not liked by many, he was    #\n#     known for his skill in deflecting blows and              #\n#     countering swiftly and deadly.                           #\n#                                                              #\n#                                                              #\n#     [attack captain] It was clear now beyond a doubt that    #\n#     the captain had lured them all into a trap. Such a       #\n#     vile act of betrayal demanded blood to be payed back     #\n#     with blood in retribution.                               #\n#                                                              #\n#     [run away] Knowing that he had little chance of          #\n#     besting his superior, the young guard retreated,         #\n#     mounted one of the horses and smacked the other two      #\n#     so they would run away. This would give him some         #\n#     time to find the princess.                               #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     The captain commended the young man on his bravery,      #\n#     and asked him to help strap his armor back on. While     #\n#     fastening the leather straps on the side, the young      #\n#     guard suddenly felt a sharp sting at the side of his     #\n#     neck. The man he was helping had stabbed him with a      #\n#     blade and then pulled him closer. 'Thanks for the        #\n#     help you little shit.', he whispered as he pulled out    #\n#     the weapon and slit the young man's throat.              #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     They fought for a long time, with the young man          #\n#     compensating his inexperience with stamina and           #\n#     endurance. But the sly man eventually overcame him       #\n#     with a special attack that used the opponent's           #\n#     inertia against him. The pupil was left dumbfounded      #\n#     for a moment, with a sword's blade protruding from       #\n#     his back. His vision blurred, and with his strength      #\n#     dripping down the hilt, he fell to the ground.           #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Pushing the horse as hard as it would go, he was soon    #\n#     able to see the cart being escored by the mercenaries.   #\n#     Slowing down, he trailed them for some time unseen,      #\n#     before the forest opened up into another great field.    #\n#     The young guard had already lost his cover when he       #\n#     realized his mistake. Turning back to ride along the     #\n#     edge of the forest until the mercenaries couldn't see    #\n#     him anymore, he was dismayed by the sight of the angry   #\n#     captain rushing towards him from the back. His roars     #\n#     of anger were heard even by the mercenaries behind       #\n#     the cart. Now the young man was about to get             #\n#     attacked from two sides.                                 #\n#                                                              #\n#     [run away] This was it. The best he could do is get      #\n#     away with his life and live to report the place he       #\n#     had last seen the princess' cart. He turned his horse    #\n#     towards the open field and rode as fast as he could.     #\n#     [attack captain] Killing the man responsible for         #\n#     everyone's death was the least he could do. It was a     #\n#     gamble, but if he succeeded, it would give him time      #\n#     to hide in the forest and re-trace the tracks.           #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Being so focused on the two characters, the young        #\n#     guard only became aware of the sound of approaching      #\n#     horses when they were already close behind him. A        #\n#     few of the mercenaries took the vanguard in order to     #\n#     search for the escapee. And now they found him. A        #\n#     bloody clash was inevitable for the young man, so he     #\n#     pulled out his sword and took a fighting stance. But     #\n#     it was only his blood that stained the forest floor.     #\n#     The mercenaries, wary of the tenacity of the royal       #\n#     guard, shot a few arrows into his head and chest,        #\n#     and left the stripped body to the animals.               #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Riding deeper into the thicket on the left, he soon      #\n#     found a path a horse could follow more swiftly.          #\n#     Cautiously passing the part nearest to the two men,      #\n#     the young guard soon found himself on the other side     #\n#     of the field, and continued to follow the tracks the     #\n#     cart left behind. Rushing the horse, he traversed a      #\n#     long way until suddenly the tracks stopped.              #\n#     Dismounting, he tracked them back to a sharp turn into   #\n#     a dark thicket. Not too far off, there was a small       #\n#     overhanging hillock hiding the mercenary camp behind.    #\n#     The cart was there, near a special tent where the        #\n#     princess was probably being held.                        #\n#        [ride back] There was no way a solitary guard could   #\n#     take on so many mercenaries by himself. The young man    #\n#     decided that his mission of gathering news was           #\n#     completed, and the best thing to do is to ride back to   #\n#     the castle and inform the king and the other captains.   #\n#        [devise plan] The mercenaries might not stay here     #\n#     long enough, and then the princess' trail might be       #\n#     lost. The only way to save her was to infiltrate         #\n#     the camp somehow.                                        #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Silently retreating from the hillock, and exiting the    #\n#     unlikely thicket entrance, the young man jumped on       #\n#     his horse and rode it as fast as he could. The castle    #\n#     was not far. At least, it wouldn't be on a fresh         #\n#     horse. This poorly cared-after mercenary horse was       #\n#     foaming at the mount from the strain. Near the end it    #\n#     became more stubborn, and once it found a pond, it       #\n#     wouldn't move. The guard stripped all of his armor       #\n#     save his carved pauldron which proved his rank, and      #\n#     decided to run the rest of the way. It was already       #\n#     night when he arrived at the gates, but after hearing    #\n#     him out, and taking him to the king, a large force       #\n#     was assembed and set out before dawn. The young          #\n#     guard led them to the mercenary camp and they were       #\n#     overrun. The princess was saved and the savior was       #\n#     richly rewarded and rose in rank.                        #\n#                                                              #\n#                             END                              #\n#                                                              #\n#     [go back] To try and get a different ending.             #\n#     [main menu] Return to the main menu.                     #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     The camp was loosely defended, and there was no          #\n#     obvious order among the guards and sentries. Getting     #\n#     a diguise should be easy, and making sure the            #\n#     princess is indeed in the tent is valuable               #\n#     information for planning the next steps.                 #\n#                                                              #\n#                                                              #\n#     [get disguise] Infiltrating the camp in a royal          #\n#     guard's attire would be impossible. There are a few      #\n#     solitary drunkards walking around the outskirts of       #\n#     the camp. They should make for easy targets to strip     #\n#     and steal their garments.                                #\n#                                                              #\n#     [wait] Waiting until nighttime would probably be the     #\n#     best choice. The suspicious tent is only guarded from    #\n#     the front, and the sentries are far apart. It is         #\n#     possible that the rear area will also be dimly lit.      #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     He lost his pursuers in the next forest when he          #\n#     rushed his horse until it made a wrong step and          #\n#     collapsed down a hill. The young man broke his leg,      #\n#     and the animal was unusable. Limping, he made his        #\n#     way to the castle, fending off wild beasts along the     #\n#     way. Shivering and cold, he arrived three days later.    #\n#     The troops found the camp abandoned, and the cart        #\n#     left behind, leaving no trail for them to follow.        #\n#     The princess was married to a noble who later made       #\n#     claims at the throne. Being left a cripple, the          #\n#     young guard took his life after some time.               #\n#                                                              #\n#                                                              #\n#                             END                              #\n#                                                              #\n#                                                              #\n#     [go back] To try and get a different ending.             #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Unfortunately, the clash lasted more than a moment.      #\n#     The captain cut off his path and struck repeatedly       #\n#     with the full force of his rage. Coming from the         #\n#     rear, the mercenaries came and hacked away at the        #\n#     poor lad. His lascerated body fell off the horse,        #\n#     and his pain was cut short by a broken neck. The         #\n#     young guard's head rolled limply as the captain          #\n#     kicked it in his unsubsided fury.                        #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Sneaking up to a drunkard lying in the shade of a        #\n#     tree next to the hillock, the young man realized that    #\n#     he wouldn't need to kill him as the man was utterly      #\n#     passed out. He rolled him around and stripped his        #\n#     smelly and dirty garments. Hiding his armor in a bush    #\n#     and putting on the rags, he strolled confidently into    #\n#     the camp. No one seemed to mind him, so he went          #\n#     towards the tent to try and take a look inside.          #\n#     Suddenly, one of the men guarding the tent told him to   #\n#     come near. 'Really nice boots you have there.', he       #\n#     said with a smirk. The young man told him that he got    #\n#     them off a dead man. 'And so cleanly shaven...', the     #\n#     man marvelled. In that moment, the naked drunkard ran    #\n#     into camp shouting that someone had stolen his clothes.  #\n#     The young royal guard was quickly apprehended and tied   #\n#     to a tree. They left him there to starve when they       #\n#     moved camp. The agony lasted for several days. He was    #\n#     lucky to have died before a curious wolf spotted him.    #\n#                 AND THUS HE CAME TO HIS END                  #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable. [main menu] Return to main menu.     #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     When high fell, the young man descended from the         #\n#     hillock. Keeping to the shadows, and trying not to       #\n#     cast one himself, he made his way towards the tent       #\n#     where the princess is supposed to be kept. Crawling      #\n#     to its base, he unfastened one of the wooden pins and    #\n#     pushed his head inside. The princess was all alone,      #\n#     and crying. The bonfire in the middle of the camp        #\n#     illuminated the whole tent and one could see well-       #\n#     enough even without a candle. The young guard softly     #\n#     called to her and motioned her to be quiet, as he        #\n#     pulled himself inside. Showing him that she had been     #\n#     chained to a log hammered deeply into the ground,        #\n#     she started crying again. He asked her where the         #\n#     keys are, and she said the guards had them.              #\n#                                                              #\n#          [steal keys] With no other way to free the          #\n#     princess, he knew he had to take the risk and try to     #\n#     steal the keys from the unsuspecting guards.             #\n#          [dig log] Even though digging out such a            #\n#     thick log might take a while, it was definitely less     #\n#     risky than trying to get the keys.                       #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Peeking out the tent entrance, the young man saw the     #\n#     keys dangling at the guard's waist. Making a small       #\n#     hole in the tent to have a better reach, he tried to     #\n#     unfasten them. Suddenly, one of the guards grabbed       #\n#     his hand and pulled it outside, while the other one      #\n#     made a wide swing with his axe and struck it at the      #\n#     elbow. With a shriek, the young guard pulled it back,    #\n#     only to see it dangling limply. He was captured and      #\n#     beaten, and left in a dung hole to bleed out.            #\n#                                                              #\n#                                                              #\n#                 AND THUS HE CAME TO HIS END                  #\n#                                                              #\n#                                                              #\n#     [go back] Return to a previous decision where death      #\n#     is still avoidable.                                      #\n#                                                              #\n#     [main menu] Return to the main menu.                     #\n#                                                              #\n#                                                              #\n#                                                              #\n################################################################\n"},
{"################################################################\n#                                                              #\n#     Pulling out his sword, the young guard struck it into    #\n#     the ground around the log, trying to loosen it up.       #\n#     After a lot of digging, his sword was bent in several    #\n#     places, but the princess was free. Still being           #\n#     chained to the log, she had to carry the heavy thing     #\n#     with herself. The young man helped her get out of the    #\n#     tent, and carried the log part of the way towards the    #\n#     horse he had hidden away beforehand. Even if the         #\n#     sentries saw them, it would be too late before they      #\n#     could raide an alarm. Rushing to the horse, they         #\n#     mounted it, and rode to the wide field where they        #\n#     could see their path in the moonlight. No one pursued    #\n#     them, and they reached the king's castle by midday.      #\n#     The king sent his army to find and annihilate the        #\n#     mercenary band, and bring the captain back for           #\n#     torture. The princess was safely returned, and the       #\n#     young guard was made a noble and given his own land.     #\n#                                                              #\n#                             END                              #\n#     [go back] To try and get a different ending.             #\n#     [main menu] Return to the main menu.                     #\n################################################################\n"}
	};

	const int decision_reference[][2] = {  //(number of choices, index in corresponding decision type array)
		{0, 0}, //this is filler. it should never run
		{4, 0},
		{3, 0},
		{3, 1},
		{3, 2},
		{2, 0},
		{2, 1},
		{2, 2},
		{2, 3},
		{2, 4},
		{2, 5},
		{2, 6},
		{2, 7},
		{3, 3},
		{2, 8},
		{2, 9},
		{2, 10},
		{2, 11},
		{2, 12},
		{2, 13},
		{2, 14},
		{2, 15},
		{2, 16},
		{2, 17},
		{2, 18},
		{2, 19},
		{2, 20},
		{2, 21},
		{2, 22},
		{2, 23},
		{2, 24}
	};

	const char two_decision_strings[][2][20] = {
		{"hide", "fight"},
		{"stay hidden", "rush out"},
		{"stay", "follow cart"},
		{"go back", "main menu"},
		{"go back", "main menu"},
		{"go back", "main menu"},
		{"hide and observe", "rush them"},
		{"keep observing", "move on"},
		{"threaten captain", "aid captain"},
		{"go back", "main menu"},
		{"go back", "main menu"},
		{"attack captain", "run away"},
		{"go back", "main menu"},
		{"go back", "main menu"},
		{"run away", "attack captain"},
		{"go back", "main menu"},
		{"ride back", "devise plan"},
		{"go back", "main menu"},
		{"get disguise", "wait"},
		{"go back", "main menu"},
		{"go back", "main menu"},
		{"go back", "main menu"},
		{"steal keys", "dig log"},
		{"go back", "main menu"},
		{"go back", "main menu"}
	};

	const int two_decision_indexes[][2] = {  //the index of the next scene depending on choice
		{6, 7},
		{8, 9},
		{10, 11},
		{5, 0},
		{5, 0},
		{7, 0},
		{12, 13},
		{21, 22},
		{17, 18},
		{13, 0},
		{13, 0},
		{19, 20},
		{14, 0},
		{17, 0},
		{25, 26},
		{12, 0},
		{23, 24},
		{11, 0},
		{27, 28},
		{11, 0},
		{20, 0},
		{24, 0},
		{29, 30},
		{28, 0},
		{11, 0}
	};

	const int two_decision_string_length[][2] = {
		{4, 5},
		{11, 8},
		{4, 11},
		{7, 9},
		{7, 9},
		{7, 9},
		{16, 9},
		{14, 7},
		{16, 11},
		{7, 9},
		{7, 9},
		{14, 8},
		{7, 9},
		{7, 9},
		{8, 14},
		{7, 9},
		{9, 11},
		{7, 9},
		{12, 4},
		{7, 9},
		{7, 9},
		{7, 9},
		{10, 7},
		{7, 9},
		{7, 9}
	};

	const char three_decision_strings[][3][20] = {
		{"observe cart", "observe bridge", "wait"},
		{"observe captain", "observe bridge", "wait"},
		{"observe captain", "observe cart", "wait"},
		{"attack mercenary", "attack captain", "demand answers"}
	};

	const int three_decision_indexes[][3] = {
		{3, 4, 5},
		{2, 4, 5},
		{2, 3, 5},
		{14, 15, 16}
	};

	const int three_decision_string_length[][3] = {
		{12, 14, 4},
		{15, 14, 4},
		{15, 12, 4},
		{16, 14, 14}
	};

	const char four_decision_strings[][4][20] = {
		{"observe captain", "observe cart", "observe bridge", "wait"}
	};

	const int four_decision_indexes[][4] = {
		{2, 3, 4, 5}
	};

	const int four_decision_string_length[][4] = {
		{15, 12, 14, 4}
	};


/*########################################################################
#############################   FUNCTIONS   ##############################
########################################################################*/

	void get_input() {
		fgets(user_input, user_input_char_limit, stdin);

		//remove trailing newline that fgets puts at the end
		for (int c = user_input_char_limit - 1; c >= 0; c--) {
		    if (user_input[c] == '\n') {
		        user_input[c] = '\0';
		        break;
		    }
		}
	}

	int equals_user_input(const char string[], int string_size) {
		if (string_size > user_input_char_limit) {
			return 0;
		}
		for (int ix = string_size - 1; ix >= 0; ix--) {
			if (string[ix] != user_input[ix]) {
				return 0;
			}
		}
		return 1;
	}

	int encrypt(int e) {
		// main number is shifted 2 to the left with 100, and we add a validity check number - sum of two rightmost original numbers + 41
		return (((e * 17) + 11) * 100) + ((e % 100) / 10) + (e % 10) + 41;
	}

	int decrypt(int d) {
		int validity_check = (d % 100) - 41;  //the sum of the two last digits of the decrypted number
        int decrypted_num = ((d / 100) - 11) / 17;
        if (validity_check == (((decrypted_num % 100) / 10) + (decrypted_num % 10))) {
            return decrypted_num;
        }
        else {
            return -1;
        }  
	}

    int convert_string_to_int(char st[]) {
        int result = 0;
        for(int s = 0; s < user_input_char_limit; s++) {
            if (st[s] != '\0') {
                result = (result * 10) + (st[s] - '0'); //subtracting 0 (code 48), you get the value of the number. 49(1) - 48(0) = 1
            }
            else {
                break;
            }
        }
        return result;
    } 

	int process_special() {
		if (equals_user_input("quit", 4) == 1) {
			if (current_scene != 0) {
				while (equals_user_input("y", 1) == 0 && equals_user_input("n", 1) == 0) {
					printf("Do you want to generate a savegame code? [y]/[n]\n");
					get_input();
				}
				if (equals_user_input("y", 1) == 1) {
					printf("Save code: %i\n", encrypt(current_scene));
				}
			}
			printf("Thanks for playing! Bye!\n");
			quit = 1;	
		}
		else if (equals_user_input("help", 4) == 1) {
			printf("################################################################\n#                                                              #\n#            ################################                  #\n#             ######### # ##   ## ####   #####                 #\n#              ######## # ## #### #### # ######                #\n#               #######   ##  ### ####   #######               #\n#                ###### # ## #### #### ##########              #\n#                 ##### # ##   ##   ## ###########             #\n#                  ################################            #\n#                                                              #\n#                                                              #\n#     To navigate the game, type the commands found in         #\n#     square brackets and press Enter to confirm them.         #\n#                                                              #\n#     GAME-WIDE COMMANDS:                                      #\n#     [help] Takes you to this screen.                         #\n#     [start] Start or restart the game.                       #\n#     [continue] Input a code to load a specific screen.       #\n#     [quit] Exit the program. Offers to generate a code.      #\n#                                                              #\n#                                                              #\n#     [q] Exit help screen and return to the previous one.     #\n#                                                              #\n################################################################\n");
			get_input();
			while (equals_user_input("q", 1) == 0) {
				printf("Unknown command. Write [q] to exit help!\n");
				get_input();
			}
		}
		else if (equals_user_input("continue", 8) == 1) {
			printf("Enter your secret code:\n");
			get_input();
			int decrypted_num = decrypt(convert_string_to_int(user_input));
			while(decrypted_num == -1) {  //decrypt returns -1 if bad result
				printf("Wrong code! Type a valid one or write [back] to return.\n");
				get_input();
				if (equals_user_input("back", 4) == 0) {
					decrypted_num = decrypt(convert_string_to_int(user_input));
				}
				else {
					return 1;  //tells the program that the command is processed, and in the next loop, the current scene will load
				}
			}
			current_scene = decrypted_num;
		}
		else if (equals_user_input("start", 5) == 1) {
			if (current_scene == 0) {
				current_scene = 1; //beginning scene
			}
			else {
				while (equals_user_input("y", 1) == 0 && equals_user_input("n", 1) == 0) {
					printf("Would you like to restart the game? [y]/[n]\n");
					get_input();
				}
				if (equals_user_input("y", 1) == 1) {
					current_scene = 1; //beginning scene
				}
			}
		}
		else {
			return 0;
		}
		return 1;  //returns 1 if one of the commands processes. Otherwise, else returns 0.
	}

	int process_command() {
		int array_num = decision_reference[current_scene][0];
		int internal_index = decision_reference[current_scene][1];
		if (array_num == 2) {
			for (int i = 0; i < 2; i++) {
				if (equals_user_input(two_decision_strings[internal_index][i], two_decision_string_length[internal_index][i])) {
					current_scene = two_decision_indexes[internal_index][i];
					return 1;
				}
			}
		}
		else if (array_num == 3) {
			for (int i = 0; i < 3; i++) {
				if (equals_user_input(three_decision_strings[internal_index][i], three_decision_string_length[internal_index][i])) {
					current_scene = three_decision_indexes[internal_index][i];
					return 1;
				}
			}
		}
		else if (array_num == 4) {
			for (int i = 0; i < 4; i++) {
				if (equals_user_input(four_decision_strings[internal_index][i], four_decision_string_length[internal_index][i])) {
					current_scene = four_decision_indexes[internal_index][i];
					return 1;
				}
			}
		}
		return 0;
	}


/*########################################################################
###############################   PROGRAM   ##############################
########################################################################*/

int main() {
	while (quit == 0) {  //main loop
		int process_finished = 0;
		
		printf("%s", scenes[current_scene]);
		get_input();

		while (process_finished == 0) {
			if (process_special() == 0) {  //if the input wasn't a special command
				if (process_command() == 1) {  //if the user input was valid
					process_finished = 1;
				}
				else {
					printf("Wrong command. Type 'help' for a list.\n");
					get_input();
				}
			}
			else {
				process_finished = 1;
			}
		}
	}

	return 0;
}



